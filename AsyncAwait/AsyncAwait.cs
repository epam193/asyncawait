﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsyncAwaitProject
{
    public static class AsyncAwait
    {
        public static async Task<List<int>> Task1Async()
        {
            return await Task.Run(() =>
            {
                var array = new List<int>();
                Random random = new Random();
                for (int i = 0; i < 10; i++)
                {
                    array.Add(random.Next(0, 100));
                }
                string data = "Task1:" + string.Join(" - ", array);
                Console.WriteLine(data);
                return array;
            });

        }

        public static async Task<List<int>> Task2Async(List<int> list)
        {
            return await Task.Run(() =>
            {
                if (!ValidationParameter(list))
                {
                    return new List<int>();
                }

                Random random = new Random();
                int randomNumber = random.Next(0, 10);
                var array = list.Select(x => x * randomNumber).ToList();
                string data = "Task2:" + string.Join(" - ", array);
                Console.WriteLine(data);
                return array;
            });
        }

        public static async Task<List<int>> Task3Async(List<int> list)
        {
            return await Task.Run(() =>
            {
                if (!ValidationParameter(list))
                {
                    return new List<int>();
                }

                var array = list.OrderBy(x => x).ToList();
                string data = "Task3:" + string.Join(" - ", array);
                Console.WriteLine(data);
                return array;
            });     
        }

        public static async Task<double> Task4Async(List<int> list)
        {
            return await Task.Run(() =>
            {
                double avg = list.Average(x => x);
                Console.WriteLine("Task4:" + avg);
                return avg;
            });
        }


        private static bool ValidationParameter(List<int> list)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list), "list should not be null.");
            }

            if (list.Count != 10)
            {
                throw new ArgumentException("list items must be equals to 10.", nameof(list));
            }
            return true;
        }
    }
}
