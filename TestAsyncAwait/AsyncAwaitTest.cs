using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AsyncAwaitProject;

namespace TestAsyncAwait
{
    public static class AsyncAwaitTest
    {
        /// <summary>
        /// Task 1: creates an array of 10 random integers.
        /// </summary>
        /// <returns>Test1.</returns>
        [Test]
        public static async Task Test1()
        {
            var task = await AsyncAwait.Task1Async();
            Assert.IsTrue(task.Count > 0);
        }

        /// <summary>
        /// Task 1: creates an array of 10 random integers.
        /// Task 2: multiplies the array by a randomly generated number.
        /// </summary>
        /// <returns>Without Params Test.</returns>
        [Test]
        public static async Task Test2WithoutParam()
        {
            var task = AsyncAwait.Task1Async();

            _ = task.ContinueWith(
                async result =>
                {
                    _ = AsyncAwait.Task2Async(await task);
                }, TaskContinuationOptions.RunContinuationsAsynchronously);
            Assert.IsTrue(task.Result.Count > 0);
        }

        /// <summary>
        /// Task 1: creates an array of 10 random integers.
        /// Task 2: multiplies the array by a randomly generated number.
        /// Task 3: sorts the array by ascending.
        /// </summary>
        /// <returns>Without Params Test.</returns>
        [Test]
        public static async Task Test3WithoutParam()
        {
            var task = AsyncAwait.Task1Async();

            _ = task.ContinueWith(
                async result =>
                {
                    _ = AsyncAwait.Task2Async(await task);
                }, TaskContinuationOptions.RunContinuationsAsynchronously);

            _ = task.ContinueWith(
                async result =>
                {
                    _ = AsyncAwait.Task3Async(await task);
                }, TaskContinuationOptions.RunContinuationsAsynchronously);

            Assert.IsTrue(task.Result.Count > 0);
        }

        /// <summary>
        /// Task 1: creates an array of 10 random integers.
        /// Task 2: multiplies the array by a randomly generated number.
        /// Task 3: sorts the array by ascending.
        /// Task 4: calculates the average value.
        /// </summary>
        /// <returns>Without Params Test.</returns>
        [Test]
        public static async Task Test4WithoutParam()
        {
            var task = AsyncAwait.Task1Async();

            _ = task.ContinueWith(
                async result =>
                {
                    _ = AsyncAwait.Task2Async(await task);
                }, TaskContinuationOptions.RunContinuationsAsynchronously);

            _ = task.ContinueWith(
                async result =>
                {
                    _ = AsyncAwait.Task3Async(await task);
                }, TaskContinuationOptions.RunContinuationsAsynchronously);

            _ = task.ContinueWith(
                async result =>
                {
                    _ = AsyncAwait.Task4Async(await task);
                }, TaskContinuationOptions.RunContinuationsAsynchronously);

            Assert.IsTrue(task.Result.Count > 0);
        }

        [TestCase(null)]
        [TestCase(1, 2, 3, 4, 5, 6, 7, 8, 9)]
        [TestCase(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)]
        [TestCase(1, 21, 33, 49, 54, 63, 70, 88, 99, 14)]
        public static async Task Test2(params int[] listArray)
        {
            List<int> list = new List<int>(listArray);

            //Act
            try
            {
                var result = await AsyncAwait.Task2Async(list);
                Assert.AreEqual(10, result.Count);
            }
            //Assert
            catch (ArgumentNullException)
            {
                Assert.IsNull(list);
            }
            catch (ArgumentException)
            {
                Assert.IsTrue(list.Count != 10);
            }
        }

        [TestCase(null)]
        [TestCase(12, 55, 65, 47, 74, 48, 89)]
        [TestCase(10, 15, 22, 34, 45, 57, 64, 79, 88)]
        [TestCase(21, 25, 32, 56, 66, 72, 84, 90, 95, 99)]
        public static async Task Test3(params int[] listArray)
        {
            List<int> list = new List<int>(listArray);

            //Act
            try
            {
                var result = await AsyncAwait.Task3Async(list);
                var expected = list.OrderBy(x => x).ToList();
                Assert.AreEqual(expected, result);
            }
            //Assert
            catch (ArgumentNullException)
            {
                Assert.IsNull(list);
            }
            catch (ArgumentException)
            {
                Assert.IsTrue(list.Count != 10);
            }
        }

        [TestCase(null)]
        [TestCase(12, 55, 65, 47, 74, 48, 89)]
        [TestCase(10, 15, 22, 34, 45, 57, 64, 79, 88)]
        [TestCase(21, 25, 32, 56, 66, 72, 84, 90, 95, 99)]
        public static async Task Test4(params int[] listArray)
        {
            List<int> list = new List<int>(listArray);

            //Act
            try
            {
                var result = await AsyncAwait.Task4Async(list);
                var expected = list.Average(x => x);
                Assert.AreEqual(expected, result);
            }
            //Assert
            catch (ArgumentNullException)
            {
                Assert.IsNull(list);
            }
            catch (ArgumentException)
            {
                Assert.IsTrue(list.Count != 10);
            }
        }
    }
}